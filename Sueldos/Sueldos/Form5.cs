﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Negocio;

namespace Sueldos
{
    public partial class Form5 : Form 
    {
        public Form5()
        {
            InitializeComponent();
        }

        private void Form5_Load(object sender, EventArgs e)
        {
            listarEmpleados();
            cargarMesesyAnios();
           
        }

        void listarEmpleados()
        {

            grillaEmpleados.DataSource = null;
            grillaEmpleados.DataSource = Empleado.listarEmpleado();
        
        }

        private void grillaEmpleados_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            textBox3.Text = grillaEmpleados.Rows[e.RowIndex].Cells[0].Value.ToString();
            textBox1.Text = grillaEmpleados.Rows[e.RowIndex].Cells[2].Value.ToString();
            textBox2.Text = grillaEmpleados.Rows[e.RowIndex].Cells[1].Value.ToString();
        }


        void cargarMesesyAnios()
        {
            comboBox1.Items.Add("");
            comboBox1.Items.Add("Enero");
            comboBox1.Items.Add("Febrero");
            comboBox1.Items.Add("Marzo");
            comboBox1.Items.Add("Abril");
            comboBox1.Items.Add("Mayo");
            comboBox1.Items.Add("Junio");
            comboBox1.Items.Add("Julio");
            comboBox1.Items.Add("Agosto");
            comboBox1.Items.Add("Septiembre");
            comboBox1.Items.Add("Octubre");
            comboBox1.Items.Add("Noviembre");
            comboBox1.Items.Add("Diciembre");

            comboBox2.Items.Add("");
            comboBox2.Items.Add("2021");
            comboBox2.Items.Add("2020");
            comboBox2.Items.Add("2019");
            comboBox2.Items.Add("2018");
            comboBox2.Items.Add("2017");
            comboBox2.Items.Add("2016");
            comboBox2.Items.Add("2015");

        }

        private void button1_Click(object sender, EventArgs e)
        {
            EnlanzarRECIBOS();
            grillaconceptodelrecibo.DataSource = null;
        }


        private void EnlanzarRECIBOS()
        {
            if ((comboBox1.Text == "") && (comboBox2.Text == ""))//mes
            {
                grillaRecibos.DataSource = null;
                grillaRecibos.DataSource = Recibo.ListarReciboPorEmpleado(textBox3.Text); //id empleado
            }
            else
            {
                if ((comboBox1.Text == "") || (comboBox2.Text == ""))
                {
                    MessageBox.Show("El mes y año deben estar completos");
                }
                else
                {
                    if ((comboBox1.Text != "") && (comboBox2.Text != ""))
                    {

                        grillaRecibos.DataSource = null;
                        grillaRecibos.DataSource = Recibo.ListarReciboPorEmpleadoMesAnio(textBox3.Text, comboBox1.Text, comboBox2.Text); //mes y año

                    }
                }
            }
        }

        private void grillaRecibos_CellClick(object sender, DataGridViewCellEventArgs e)
        {
             int id_recibo;
             id_recibo = int.Parse(grillaRecibos.Rows[e.RowIndex].Cells[0].Value.ToString());
             grillaconceptodelrecibo.DataSource = null;
             grillaconceptodelrecibo.DataSource = Recibo.ConceptosPorRecibo(id_recibo);
        }
      
                
      }
}
