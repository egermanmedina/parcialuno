﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Negocio;



namespace Sueldos
{
    public partial class Form3 : Form
    {
        public Form3()
        {
            InitializeComponent();
        }

        void EnlazarConceptos()
        {

            grillaConceptos.DataSource = null;
            grillaConceptos.DataSource = Conceptos.listarConceptos(); 

        }

        private void grillaConceptos_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            textBox1.Text = grillaConceptos.Rows[e.RowIndex].Cells[0].Value.ToString();
            textBox2.Text = grillaConceptos.Rows[e.RowIndex].Cells[1].Value.ToString();
            textBox3.Text = grillaConceptos.Rows[e.RowIndex].Cells[2].Value.ToString();
        }

        private void Form3_Load(object sender, EventArgs e)
        {
            EnlazarConceptos();
        }

        private void button2_Click(object sender, EventArgs e)
        {


            if (textBox3.Text == "")//mes
            {
                MessageBox.Show("El Porcentaje debe ser completado");
            }

            else
            {
                Conceptos concepto = new Conceptos();
                concepto.DescripcionConcepto = textBox2.Text;
                concepto.Porcentaje = float.Parse(textBox3.Text);
                concepto.Insertar();
                EnlazarConceptos();

            }
         
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (textBox3.Text == "")//mes
            {
                MessageBox.Show("El Porcentaje debe contener un valor");
            }
            else
            {

                Conceptos concepto = new Conceptos();
                concepto.Id = int.Parse(textBox1.Text);
                concepto.DescripcionConcepto = textBox2.Text;
                concepto.Porcentaje = float.Parse(textBox3.Text);
                concepto.Editar();
                EnlazarConceptos();

            }
           
        }

        private void button3_Click(object sender, EventArgs e)
        {
            Conceptos concepto = new Conceptos();
            concepto.Id = int.Parse(textBox1.Text);
            concepto.Borrar();
            EnlazarConceptos();
        }




    }
}
