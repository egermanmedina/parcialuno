﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Negocio;

namespace Sueldos
{
    public partial class Form2 : Form
    {
        public Form2()
        {
            InitializeComponent();
        }

        void EnlazarEmpleados()
        {

            grillaEmpleado.DataSource = null;
            grillaEmpleado.DataSource = Empleado.listarEmpleado();

        }

        private void button1_Click(object sender, EventArgs e)
        {
            Empleado Empleado = new Empleado();
            Empleado.NombreApellido = textBox2.Text;
            Empleado.Legajo = int.Parse(textBox1.Text);
            Empleado.Cuil = int.Parse(textBox3.Text);
            Empleado.FechaAlta = textBox4.Text;
            Empleado.Insertar();
            EnlazarEmpleados();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            Empleado Empleado = new Empleado();
            Empleado.Id = int.Parse(textBox5.Text);
            Empleado.NombreApellido = textBox2.Text;
            Empleado.Legajo = int.Parse(textBox1.Text);
            Empleado.Cuil = int.Parse(textBox3.Text);
            Empleado.FechaAlta = textBox4.Text;
            Empleado.Editar();
            EnlazarEmpleados();
            

        }

        private void grillaEmpleado_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            textBox5.Text = grillaEmpleado.Rows[e.RowIndex].Cells[0].Value.ToString();
            textBox1.Text = grillaEmpleado.Rows[e.RowIndex].Cells[1].Value.ToString();
            textBox2.Text = grillaEmpleado.Rows[e.RowIndex].Cells[2].Value.ToString();
            textBox3.Text = grillaEmpleado.Rows[e.RowIndex].Cells[3].Value.ToString();
            textBox4.Text = grillaEmpleado.Rows[e.RowIndex].Cells[4].Value.ToString();
           



        }

        private void Form2_Load(object sender, EventArgs e)
        {
            EnlazarEmpleados();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            Empleado Empleado = new Empleado();
            Empleado.Id = int.Parse(textBox5.Text);
            Empleado.Borrar();
            EnlazarEmpleados();


        }

    
    }
}
