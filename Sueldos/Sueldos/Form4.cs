﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Negocio;


namespace Sueldos
{
    public partial class Form4 : Form
    {
        public Form4()
        {
            InitializeComponent();
        }

        void EnlazarEmpleadosyConceptos()
        {
            
             grillaseleccionEmpleados.DataSource = null;
             grillaseleccionEmpleados.DataSource = Empleado.listarEmpleado();
             grillaseleccionConceptos.DataSource = null;
             grillaseleccionConceptos.DataSource = Conceptos.listarConceptos();
        }

        private void Form4_Load(object sender, EventArgs e)
        {
            EnlazarEmpleadosyConceptos();
            cargarMesesyAnios();
        }

        private void grillaseleccionEmpleados_CellClick(object sender, DataGridViewCellEventArgs e)
        {
                textBox1.Text = grillaseleccionEmpleados.Rows[e.RowIndex].Cells[1].Value.ToString();
                textBox2.Text = grillaseleccionEmpleados.Rows[e.RowIndex].Cells[2].Value.ToString();
                textBox3.Text = grillaseleccionEmpleados.Rows[e.RowIndex].Cells[4].Value.ToString();
                textBox4.Text = grillaseleccionEmpleados.Rows[e.RowIndex].Cells[3].Value.ToString();
                textBox9.Text = grillaseleccionEmpleados.Rows[e.RowIndex].Cells[0].Value.ToString();
                
        }

        private void button1_Click(object sender, EventArgs e)
        {
            conceptosSeleccionados.Rows.Add(new string[] {
                Convert.ToString(grillaseleccionConceptos[1, grillaseleccionConceptos.CurrentRow.Index].Value),
                Convert.ToString(grillaseleccionConceptos[2, grillaseleccionConceptos.CurrentRow.Index].Value),
                Convert.ToString(grillaseleccionConceptos[0, grillaseleccionConceptos.CurrentRow.Index].Value)
            });
        }

        private void button4_Click(object sender, EventArgs e)
        {
            if (conceptosSeleccionados.RowCount > 1)
            {
                conceptosSeleccionados.Rows.Remove(conceptosSeleccionados.CurrentRow);
            } 

        }

        void cargarMesesyAnios()
        {
            comboBox1.Items.Add("Enero");
            comboBox1.Items.Add("Febrero");
            comboBox1.Items.Add("Marzo");
            comboBox1.Items.Add("Abril");
            comboBox1.Items.Add("Mayo");
            comboBox1.Items.Add("Junio");
            comboBox1.Items.Add("Julio");
            comboBox1.Items.Add("Agosto");
            comboBox1.Items.Add("Septiembre");
            comboBox1.Items.Add("Octubre");
            comboBox1.Items.Add("Noviembre");
            comboBox1.Items.Add("Diciembre");

            comboBox2.Items.Add("2021");
            comboBox2.Items.Add("2020");
            comboBox2.Items.Add("2019");
            comboBox2.Items.Add("2018");
            comboBox2.Items.Add("2017");
            comboBox2.Items.Add("2016");
            comboBox2.Items.Add("2015");


        
        }

        private void button2_Click(object sender, EventArgs e)
        {
            decimal porcentaje = 0;
            decimal sueldoBruto = Convert.ToDecimal(textBox5.Text);
            decimal sueldoNeto;
            decimal totalDescuento;

      

            foreach (DataGridViewRow row in conceptosSeleccionados.Rows) 
            {
                porcentaje += Convert.ToDecimal(row.Cells[1].Value);
                
            
            }

            textBox7.Text = Convert.ToString(porcentaje);
              totalDescuento = porcentaje * sueldoBruto / 100;
              textBox8.Text = Convert.ToString(totalDescuento);
              sueldoNeto = sueldoBruto - totalDescuento;
              textBox6.Text = Convert.ToString(sueldoNeto);
            
            
            
            
         }

        private void button3_Click(object sender, EventArgs e)
        {

           
           Recibo Rec = new Recibo();
            
            
            Rec.Mes = comboBox1.Text;
            Rec.Anio = comboBox2.Text;
            Rec.SueldoBruto = float.Parse(textBox5.Text);
            Rec.SueldoNeto = float.Parse(textBox6.Text);
            Rec.TotalDescuento = float.Parse(textBox8.Text);
            Rec.IdEmpleado = int.Parse(textBox9.Text);
            Rec.InsertarRecibo();

           

            int concepto;
            foreach (DataGridViewRow row in conceptosSeleccionados.Rows)
            {
                concepto = Convert.ToInt32(row.Cells[2].Value);
                Rec.InsertarReciboConcepto(concepto);

            }

            
            
        }

       
    }
}
