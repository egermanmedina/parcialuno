﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ACCESO;
using System.Data;
using System.Data.SqlClient;


namespace Negocio
{
    public class Empleado
    {

        private int id;

        public int Id
        {
            get { return id; }
            set { id = value; }

        }
        private int legajo;

        public int Legajo
        {
            get { return legajo; }
            set { legajo = value; }
        
        }

        private string nombreapellido;

        public string NombreApellido
        {
            get { return nombreapellido; }
            set { nombreapellido = value; }

        }

        private int cuil;

        public int Cuil
        {
            get { return cuil; }
            set { cuil = value; }

        }

        private string fechaalta;

        public string FechaAlta
        {
            get { return fechaalta; }
            set { fechaalta = value; }

        }




        private Acceso acceso = new Acceso();

        public void Insertar()
        {
            acceso.Abrir();
            List<IDbDataParameter> parameters = new List<IDbDataParameter>();
            parameters.Add(acceso.CrearParametro("@leg", this.legajo));
            parameters.Add(acceso.CrearParametro("@nom", this.nombreapellido));
            parameters.Add(acceso.CrearParametro("@cuil", this.cuil));
            parameters.Add(acceso.CrearParametro("@fecha", this.fechaalta));
            acceso.Escribir("EMPLEADO_INSERTAR", parameters);


            acceso.Cerrar();

           
        }

        public void Editar()
        {

            acceso.Abrir();
            List<IDbDataParameter> parameters = new List<IDbDataParameter>();
            parameters.Add(acceso.CrearParametro("@nom", this.nombreapellido));
            parameters.Add(acceso.CrearParametro("@cuil", this.cuil));
            parameters.Add(acceso.CrearParametro("@fecha", this.fechaalta));
            parameters.Add(acceso.CrearParametro("@leg", this.legajo));
            parameters.Add(acceso.CrearParametro("@id", this.id));
            
            acceso.Escribir("EMPLEADO_EDITAR", parameters);


            acceso.Cerrar();


        }

        public void Borrar()
        {
            acceso.Abrir();
            List<IDbDataParameter> parameters = new List<IDbDataParameter>();
            parameters.Add(acceso.CrearParametro("@id", this.id));
            acceso.Escribir("EMPLEADO_BORRAR", parameters);


            acceso.Cerrar();

        }

        public static List<Empleado> listarEmpleado()
        {

            List<Empleado> lista = new List<Empleado>();
            Acceso acceso = new Acceso();
            acceso.Abrir();
            DataTable tabla = acceso.Leer("EMPLEADO_LISTAR");
            acceso.Cerrar();

            foreach (DataRow registro in tabla.Rows)
            {

                Empleado E = new Empleado();
                E.id = int.Parse(registro[0].ToString());
                E.nombreapellido = registro[2].ToString();
                E.legajo = int.Parse(registro[1].ToString());
                E.cuil = int.Parse(registro[3].ToString());
                E.FechaAlta = registro[4].ToString();
                lista.Add(E);
            
            }

            return lista;
        
        }

  


      

     

      

    }
}
