﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ACCESO;
using System.Data;
using System.Data.SqlClient;


namespace Negocio
{
   public class Conceptos
    {

        private int id;

        public int Id
        {
            get { return id; }
            set { id = value; }

        }

        private string descripcionconcepto;

        public string DescripcionConcepto
        {
            get { return descripcionconcepto; }
            set { descripcionconcepto = value; }

        }

        private float porcentaje;

        public float Porcentaje
        {
            get { return porcentaje; }
            set { porcentaje = value; }

        }


        private Acceso acceso = new Acceso();

        public void Insertar()
        {
            acceso.Abrir();
            List<IDbDataParameter> parameters = new List<IDbDataParameter>();
            parameters.Add(acceso.CrearParametro("@porcentaje", this.porcentaje));
            parameters.Add(acceso.CrearParametro("@nom", this.descripcionconcepto));
            acceso.Escribir("CONCEPTO_INSERTAR", parameters);


            acceso.Cerrar();


        }

        public void Editar()
        {

            acceso.Abrir();
            List<IDbDataParameter> parameters = new List<IDbDataParameter>();
            parameters.Add(acceso.CrearParametro("@nom", this.descripcionconcepto));
            parameters.Add(acceso.CrearParametro("@porcentaje", this.porcentaje));
            parameters.Add(acceso.CrearParametro("@id", this.id));

            acceso.Escribir("CONCEPTO_EDITAR", parameters);


            acceso.Cerrar();


        }

        public void Borrar()
        {
            acceso.Abrir();
            List<IDbDataParameter> parameters = new List<IDbDataParameter>();
            parameters.Add(acceso.CrearParametro("@id", this.id));
            acceso.Escribir("CONCEPTO_BORRAR", parameters);


            acceso.Cerrar();

        }

        public static List<Conceptos> listarConceptos()
        {

            List<Conceptos> lista = new List<Conceptos>();
            Acceso acceso = new Acceso();
            acceso.Abrir();
            DataTable tabla = acceso.Leer("CONCEPTOS_LISTAR");
            acceso.Cerrar();

            foreach (DataRow registro in tabla.Rows)
            {

                Conceptos C = new Conceptos();
                C.id = int.Parse(registro[0].ToString());
                C.descripcionconcepto = registro[1].ToString();
                C.porcentaje = float.Parse(registro[2].ToString());
                lista.Add(C);

            }

            return lista;

        }
    }
}
