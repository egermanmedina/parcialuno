﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ACCESO;
using System.Data;
using System.Data.SqlClient;

namespace Negocio
{
    public class Recibo
    {
        private int id;
        public int Id
        {
            get {return id;}
            set { id = value; }

        
        }

        private string mes;
        public string Mes
        {
            get { return mes; }
            set { mes = value; }


        }

        private string anio;
        public string Anio
        {
            get { return anio; }
            set { anio = value; }


        }

        private float sueldobruto;
        public float SueldoBruto
        {
            get { return sueldobruto; }
            set { sueldobruto = value; }


        }

        private float sueldoneto;
        public float SueldoNeto
        {
            get { return sueldoneto; }
            set { sueldoneto = value; }


        }

        private float totaldescuento;
        public float TotalDescuento
        {
            get { return totaldescuento; }
            set { totaldescuento = value; }


        }

        private int idempleado;
        public int IdEmpleado
        {
            get { return idempleado; }
            set { idempleado = value; }


        }


       

        

     

        public static List<Recibo> listarRecibo()
        {

            List<Recibo> lista = new List<Recibo>();
            Acceso acceso = new Acceso();
            acceso.Abrir();
            DataTable tabla = acceso.Leer("RECIBO_LISTAR");
            acceso.Cerrar();

            foreach (DataRow registro in tabla.Rows)
            {

                Recibo R = new Recibo();
               
                R.id = int.Parse(registro[0].ToString());
                R.mes = registro[1].ToString();
                R.anio = registro[2].ToString();
                R.sueldobruto = float.Parse(registro[3].ToString());
                R.sueldoneto = float.Parse(registro[4].ToString());
                R.totaldescuento = float.Parse(registro[5].ToString());
                R.idempleado = int.Parse(registro[6].ToString());
                lista.Add(R);

            }

            return lista;

        }

        private Acceso acceso = new Acceso();

       
            


        public void InsertarRecibo()
        {
            acceso.Abrir();
            List<IDbDataParameter> parameters = new List<IDbDataParameter>();
            parameters.Add(acceso.CrearParametro("@mes", this.mes));
            parameters.Add(acceso.CrearParametro("@anio", this.anio));
            parameters.Add(acceso.CrearParametro("@sueldob", this.sueldobruto));
            parameters.Add(acceso.CrearParametro("@sueldon", this.sueldoneto));
            parameters.Add(acceso.CrearParametro("@totald", this.totaldescuento));
            parameters.Add(acceso.CrearParametro("@idempleado", this.idempleado));
            acceso.Escribir("RECIBO_INSERTAR", parameters);




            acceso.Cerrar();
        
        }

        public static List<Recibo> ListarReciboPorEmpleado(string IDEmpleado)
        {
            List<Recibo> lista = new List<Recibo>();
           

            Acceso acceso = new Acceso();
            acceso.Abrir();

            List<IDbDataParameter> parameters = new List<IDbDataParameter>();
            parameters.Add(acceso.CrearParametro("@idempleado", IDEmpleado));

            DataTable tabla = acceso.Leer("RECIBO_LISTAREMPLEADO", parameters);
            acceso.Cerrar();
            // A partir de este momento estoy en memoria, ya tome los datos del Servidor.

            foreach (DataRow registro in tabla.Rows)
            {
                Recibo Rec = new Recibo();
                Rec.Id = int.Parse(registro["ID_RECIBO"].ToString());
                Rec.Mes = registro["MES"].ToString();
                Rec.Anio = registro["ANO"].ToString();
                Rec.SueldoBruto = float.Parse(registro["SUELDO_BRUTO"].ToString());
                Rec.sueldoneto = float.Parse(registro["SUELDO_NETO"].ToString());
                Rec.TotalDescuento = float.Parse(registro["TOTAL_DESCUENTO"].ToString());
                Rec.IdEmpleado = int.Parse(registro["FK_ID_EMPLEADO"].ToString());
               

                lista.Add(Rec);
            }

            return lista;
        }

        public static List<Recibo> ListarReciboPorEmpleadoMesAnio(string IDEmpleado, string mes, string anio)
        {
            List<Recibo> lista = new List<Recibo>();
           

            Acceso acceso = new Acceso();
            acceso.Abrir();

            List<IDbDataParameter> parameters = new List<IDbDataParameter>();
            parameters.Add(acceso.CrearParametro("@idempleado", IDEmpleado));
            parameters.Add(acceso.CrearParametro("@mes", mes));
            parameters.Add(acceso.CrearParametro("@anio", anio));

            DataTable tabla = acceso.Leer("RECIBO_LISTAREMPLEADOMESANIO", parameters);
            acceso.Cerrar();
            // A partir de este momento estoy en memoria, ya tome los datos del Servidor.

            foreach (DataRow registro in tabla.Rows)
            {
                Recibo Rec = new Recibo();
                Rec.Id = int.Parse(registro["ID_RECIBO"].ToString());
                Rec.Mes = registro["MES"].ToString();
                Rec.Anio = registro["ANO"].ToString();
                Rec.SueldoBruto = float.Parse(registro["SUELDO_BRUTO"].ToString());
                Rec.sueldoneto = float.Parse(registro["SUELDO_NETO"].ToString());
                Rec.TotalDescuento = float.Parse(registro["TOTAL_DESCUENTO"].ToString());
                Rec.IdEmpleado = int.Parse(registro["FK_ID_EMPLEADO"].ToString());
               

                lista.Add(Rec);
            }

            return lista;
        }


        public void InsertarReciboConcepto(int concepto)
        {

            acceso.Abrir();
            List<IDbDataParameter> parameters = new List<IDbDataParameter>();
            parameters.Add(acceso.CrearParametro("@concepto", concepto));
            acceso.Escribir("RECIBO_CONCEPTO", parameters);




            acceso.Cerrar();

        }

        

             public static List<Conceptos> ConceptosPorRecibo(int id_recibo)
        {
            List<Conceptos> listaConceptos = new List<Conceptos>();
           

            Acceso acceso = new Acceso();
            acceso.Abrir();

            List<IDbDataParameter> parameters = new List<IDbDataParameter>();
            parameters.Add(acceso.CrearParametro("@idrecibo", id_recibo));
            DataTable tabla = acceso.Leer("RECIBO_LISTARCONCEPTOS", parameters);
            acceso.Cerrar();
            // A partir de este momento estoy en memoria, ya tome los datos del Servidor.

            foreach (DataRow registro in tabla.Rows)
            {
                Conceptos CON = new Conceptos();
                CON.Id = int.Parse(registro["FK_ID_CONCEPTO"].ToString());
                CON.DescripcionConcepto = registro["DESCRIPCION"].ToString();
                CON.Porcentaje = float.Parse(registro["PORCENTAJE"].ToString());



                listaConceptos.Add(CON);
            }

            return listaConceptos;
        }




       

        


           
    }
}
